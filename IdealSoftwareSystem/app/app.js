﻿// for routing

'use strict';
angular.module('app', ["ngRoute", "ngCookies", "ngMessages"]).
    config(function ($routeProvider, $locationProvider) {

        $routeProvider.
    when('/login', {
        templateUrl: 'views/login.html',
        controller: 'loginCtrl',
    }).
    when('/signUp', {
        templateUrl: 'views/signUp.html',
        controller: 'signCtrl as signup',
    }).
    when('/changeAc', {
        templateUrl: 'views/changeWebAccountInformation.html',
        controller: 'changeAcctCtrl',
    }).
    when('/changeAcSubmit', {
        templateUrl: 'views/AccountInfoSubmit.html',
    }).
    when('/account', {
        templateUrl: 'views/yourAccount.html',
        controller: 'accountCtrl',
    }).
    when('/ReportIssue', {
        templateUrl: 'views/reportIssue.html',
        controller: 'reportIssueCtrl',
    }).
    when('/reportIssueSubmit',
    {
        templateUrl: 'views/reportIssueSubmit.html',

    }).
     when('/RetrieveLoginInfo',
     {
         templateUrl: 'views/RetrieveLoginInfo.html',
         controller: 'retrvLoginCtrl',
     }).
     when('/PaymentHistory', {
         templateUrl: 'views/paymentHistory.html',
         controller: 'paymentHistoryCtrl',
     }).
     when('/checkOut', {
         templateUrl: 'views/checkOut.html',
         controller: 'checkCtrl',
      }).
     when('/checkInfo', {
         templateUrl: 'views/checkoutAllSelected.html',
         controller: 'checkoutAllSelectedCtrl',
     }).
     when('/makePayment',
     {
         templateUrl: 'views/makePayment.html',
         controller: 'makePayCtrl',
     }).
     when('/printPayHist',
     {
         templateUrl: 'views/printPayHist.html',
         controller: 'printPaymentHistoryCtrl',
     }).
     when('/printOut',
     {
         templateUrl: 'views/printOut.html',
         controller: 'printPayCtrl',
     }).
      when('/paymentDecline',
     {
         templateUrl: 'views/paymentDeclined.html',
         controller: 'paymentDeclineCtrl',
     }).
      when('/error',
     {
         templateUrl: 'views/errorPage.html',
         controller: 'errorCtrl',
     }).
      when('/index',
     {
         templateUrl: 'index.html',
     }).
    otherwise({
        redirectTo: '/login',
    });
   
    })
    .run(function ($rootScope, $location) { //Insert in the function definition the dependencies you need.
        //Do your $on in here, like this:
        $rootScope.$on("$routeChangeStart", function (event, next, prev) {
            $rootScope.apiErrorMessage = "";
            if (sessionStorage.loggedUser == 'valid') {

                if (next.templateUrl == 'views/login.html') {
                    sessionStorage.loggedUser = 'invalid';
                    console.log(sessionStorage.loggedUser);
                }
                else {
                    if (prev.templateUrl == 'views/makePayment.html' &&
                       (next.templateUrl == 'views/checkOut.html' || next.templateUrl == 'views/checkoutAllSelected.html' ||
                        next.templateUrl == 'views/yourAccount.html' || next.templateUrl == 'changeWebAccountInformation.html' ||
                        next.templateUrl == 'views/AccountInfoSubmit.html' || next.templateUrl == 'views/signUp.html' ||
                        next.templateUrl == 'views/reportIssue.html' || next.templateUrl == 'views/RetrieveLoginInfo.html' ||
                        next.templateUrl == 'views/paymentHistory.html' || next.templateUrl == 'views/printPayHist.html' ||
                        next.templateUrl == 'views/paymentDeclined.html' || next.templateUrl == 'views/errorPage.html')) {
                        $location.path('/makePayment');
                    }
                    if (prev.templateUrl == 'views/printOut.html' && (next.templateUrl == 'views/checkOut.html' || next.templateUrl == 'views/checkoutAllSelected.html' ||
                        next.templateUrl == 'views/yourAccount.html' || next.templateUrl == 'changeWebAccountInformation.html' ||
                        next.templateUrl == 'views/AccountInfoSubmit.html' || next.templateUrl == 'views/signUp.html' ||
                        next.templateUrl == 'views/reportIssue.html' || next.templateUrl == 'views/RetrieveLoginInfo.html' ||
                        next.templateUrl == 'views/paymentHistory.html' || next.templateUrl == 'views/printPayHist.html' ||
                        next.templateUrl == 'views/paymentDeclined.html' || next.templateUrl == 'views/errorPage.html')) {
                        $location.path('/printOut');
                    }
                    if (prev.templateUrl == 'views/yourAccount.html' && next.templateUrl == 'views/login.html') {
                        $location.path('/account');
                    }
                }
            }
            else {
                if (next.templateUrl == "views/login.html" || next.templateUrl == "views/signUp.html" || next.templateUrl == "views/RetrieveLoginInfo.html" || next.templateUrl == "views/reportIssue.html" || next.templateUrl == "views/AccountInfoSubmit.html") {
                    // already going to #login, no redirect needed
                }
                else if (next.templateUrl == 'views/errorPage.html') {
                    // not going to #login, we should redirect now
                    $location.path("/error");
                }
                else {
                    $location.path("/login");
                }
            }
        });
    });
    