﻿angular.module("app").controller("mainCtrl", ["$scope", "$rootScope", function($scope, $rootScope) {
        $rootScope.isSubscribe = false
        $rootScope.ErrorMessage = "";
        $rootScope.EmaiAddress = "";
        $rootScope.phoneNo = "";
        $rootScope.storeINFO = "";

        $rootScope.privacyPolicyEnable = false;

        $rootScope.messageReceived = {
            data: {
                message: ""
            }
        };

            if(sessionStorage['privacyPolicy'] == "false"){
             $rootScope.privacyPolicyEnable = JSON.parse(sessionStorage['privacyPolicy']);
           } else {
            $rootScope.privacyPolicyEnable = true;
           }

         $rootScope.removePrivacy = function() {
            $rootScope.privacyPolicyEnable = false;
           sessionStorage['privacyPolicy'] = $rootScope.privacyPolicyEnable;
           }

        try {
            $scope.start = function() {
                $(".sidebar").toggleClass("open");
            }

            //check api response
            $scope.$watch(function() {
                return $rootScope.messageReceived;
            }, function(newVal, oldVal) {

                //check the api store is connected or not
                if (newVal.data.message === "connect") {
                    sessionStorage["customer_message"] = "";
                    $rootScope.isSubscribe = false;
                    $rootScope.message = "";
                    $rootScope.Loaded = true;
                }
                //check site_information api method response
                if (newVal.data.message === "customer_hello") {

                    $rootScope.message = "";
                    if (newVal.data.v1.success === "true") {
                        $rootScope.validToken = newVal.data.v1.errorDescription.token;
                        sessionStorage["customer_hello"] = "";
                        sessionStorage["customer_hello"] = JSON.stringify($rootScope.messageReceived);
                    } else {
                        $rootScope.message = newVal.data.v1.errorDescription;
                    }
                }
            });

        } catch (e) {
            console.log(e);
        }
    }]);