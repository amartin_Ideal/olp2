﻿angular.module("app")
    .controller("changeAcctCtrl", ["$scope", "$rootScope", "idealsApi", function($scope, $rootScope, idealsApi) {

        $scope.oldPass = "";
        $scope.emailAdd = "";
        $scope.newUserName = "";
        $scope.newpass = "";
        $scope.retyPass = "";

        try {
         $('#hamburger-inner-navbar').click(function () {
                        $("#inner-navbar").toggleClass("in");
          });
            $scope.userFLName = sessionStorage.userFLName;
            $scope.submit = function() {
                // check to make sure the form is completely valid
                $rootScope.ShowLoader = true;
                $rootScope.message = "";
                $scope.Name = "";
                var changeAc = {
                    OldPass: $scope.oldPass,
                    EmailAdd: $scope.emailAdd,
                    NewUserName: $scope.newUserName,
                    Newpass: $scope.newpass,
                    RetyPass: $scope.retyPass
                };
                //verify fields and call changeAccountReq method.
                idealsApi.changeAccountReq(changeAc.OldPass, changeAc.EmailAdd, changeAc.NewUserName, changeAc.Newpass);
            };

            $scope.paymentHistory = function() {
                idealsApi.payment_historyReq();
            }
        } catch (e) {
            console.log(e);
        }
    }]);