﻿angular.module("app").controller("startCtrl", ["$scope", "$rootScope", "$location", "idealsApi", "$cookieStore", "$state", "$timeout", function($scope, $rootScope, $location, idealsApi, $cookieStore, $state, $timeout) {
    try {

         $scope.productName = "";
         var isSelectedProd = false;
         var isSelectedDept = false;
         $scope.selectedDepartmentName = "";
         $scope.departmentsList = {};
         $scope.product = [];
         var productCode = "";
         // check api response
         $scope.list_categories = JSON.parse(sessionStorage["list_categories"] || "{}");
         var json = $scope.list_categories;
         var department = json.data.v1.departments;
         $rootScope.cartNumber = 0;
         $rootScope.IsVisible = true;
         $scope.selectedDepartment = "";
         var isTrue = false;
         $scope.departmentsList = department;
         $scope.productDetails=[];
         var comapStr="";
         var timeout;

            //------Reset cartinfo when come back from PaySuccess---------
            $rootScope.$on('$locationChangeStart', function (event, current, previous) {
            if(previous === "file:///android_asset/www/searchApp/index.html#/paySuccess")
                {
                    sessionStorage["selectedDept"] = "";
                    sessionStorage["selectedProdN"] = "";
                    sessionStorage["selectedModl"] = "";
                    sessionStorage["cartInfo"] = "";
                    $rootScope.cartNumber = 0;
                }
            });

         for (var deptL = 0; deptL <= department.length - 1; deptL++) {
             for (var prodL = 0; prodL <= department[deptL].productCodes.length - 1; prodL++) {
                 $scope.productDetails.push(department[deptL].department + " - " +department[deptL].productCodes[prodL].description);
             }
         }

       // auto search on search button to search product name.
       function autoSearchByDepartment(dept, deptDetails) {
        if(dept=="" && deptDetails==""){
          $scope.product = [];
           for (var deptL = 0; deptL <= department.length - 1; deptL++) {
                for (var prodL = 0; prodL <= department[deptL].productCodes.length - 1; prodL++) {
                    $scope.product.push(department[deptL].productCodes[prodL].description);
                }
                }
            } else {
               $scope.product = [];
               for (var deptL = 0; deptL <= department.length - 1; deptL++) {
                    for (var prodL = 0; prodL <= department[deptL].productCodes.length - 1; prodL++) {
                    var deptDetailsSplit = deptDetails.split("-");
                    $scope.product.push(department[deptL].productCodes[prodL].description);
                    var deptDetailsSplitLength = deptDetailsSplit.length;
    //               if(department[deptL].productCodes[prodL].description===(deptDetails.split("-")[1]).replace(" ",""))
                    if(department[deptL].productCodes[prodL].description === (deptDetailsSplit[deptDetailsSplitLength-1]).replace(" ","")){
    //                 idealsApi.list_modelsReq(department[deptL].productCodes[prodL].id,(deptDetails.split("-")[1]).replace(" ","")); //Written by seema it may be use if client require
                       $scope.productPcid = department[deptL].productCodes[prodL].id;
                        var selectedSearch = $("#prodName").val();
                        if(!selectedSearch){
                        $timeout(function() {
                            $timeout.cancel(mytimeout);
                            }, 1000);
                            idealsApi.list_modelsReq(department[deptL].productCodes[prodL].id, null);
                        }
                        else{
                        $timeout(function() {
                            $timeout.cancel(mytimeout);
                            }, 1000);
                            idealsApi.list_modelsReq(department[deptL].productCodes[prodL].id, selectedSearch);
                        }
//                           idealsApi.list_modelsReq(department[deptL].productCodes[prodL].id, "");
                    }
//                      $scope.product.push(department[deptL].productCodes[prodL].description);
                    }

                 }
            }

        };

        // closed sidebar when click to form
        $(document).on('click','#startForm',function(e){
        //    $(".sidebar").toggleClass("open");
            $(".sidebar").removeClass("open");
        });

        // update cart .........................
        $scope.cardUpdate = function(data) {
            var item = 0;
            $scope.cartItem = [];

            $scope.data = "";
            $scope.data = (JSON.parse(sessionStorage["cartInfo"] || "{}"));
            if ($scope.data.length == undefined || $scope.data.length == 0) {
            if(sessionStorage['cartNumber']>0){
                $rootScope.cartNumber = parseInt(sessionStorage['cartNumber']) + 1;
                sessionStorage['cartNumber'] =  $rootScope.cartNumber;
            } else {
                $scope.cartItem.push(data);
                $rootScope.cartNumber = 1;
                sessionStorage['cartNumber'] =  $rootScope.cartNumber;
            }

            } else {
                $scope.data = (JSON.parse(sessionStorage["cartInfo"] || "{}"));
                $scope.cartItem = (JSON.parse(sessionStorage["cartInfo"] || "{}"));

                for (item = 0; item <= $scope.data.length - 1; item++) {
                    if ($scope.data[item].id == data.id) {
                        break;
                    }
                }
                if (item > $scope.data.length - 1) {
                    $scope.cartItem.push(data);
                    $rootScope.cartNumber = $rootScope.cartNumber + 1;
                    sessionStorage['cartNumber'] = $rootScope.cartNumber;
                    sessionStorage["cartInfo"] = $scope.cartItem;
                }
            }
            sessionStorage["cartInfo"] = JSON.stringify($scope.cartItem);
            console.log("cartInformation--------"+ sessionStorage["cartInfo"]);
        };

        $rootScope.cart = function() {
            if ($rootScope.cartNumber == 0) {
                $state.go("cartEmpty");
            } else {
                $state.go("cartNotEmpty");
            }
        };

        //check cart info.....................................
        if (sessionStorage["cartInfo"] != "") {
            var cartinfo = JSON.parse(sessionStorage["cartInfo"] || "{}");
            $rootScope.cartNumber = 0;
//            sessionStorage['cartNumber'] = $rootScope.cartNumber;
            for (var item = 0; item <= cartinfo.length - 1; item++) {
                $rootScope.cartNumber = $rootScope.cartNumber + 1;
//                sessionStorage['cartNumber'] = $rootScope.cartNumber;
            }
        };

        if (sessionStorage['cartNumber']){
         $rootScope.cartNumber = parseInt(sessionStorage['cartNumber']);
        }

        // click on serach button to call list_model api method.......
        $scope.search = function(selectedDepartment) {
//        alert('Search')
        var selectedCategories = $("#select_value option:selected").val();
        var selectedTxtSearch = $("#prodName").val();
//        if(!selectedCategories)
          if(selectedCategories == null || selectedCategories == "" && selectedTxtSearch !="")
        {
            if ($scope.startForm.$valid) {
                $scope.selectedDepartmentName = (selectedDepartment.split("-")[0]).replace(" ","");
                var product = {
                    product_Name: $scope.productName
                };
                $rootScope.productN = product.product_Name;
                for (var item = 0; item < $scope.product.length; item++) {
                var str= ($scope.product[item]);
                comapStr=str.toUpperCase();
                var strComp= (product.product_Name).toUpperCase();
                var n= comapStr.search(strComp);
                if(n!=-1)
                {
                 isSelectedProd = true;
                 break;
                }
                else
                {
                 isSelectedProd = false;
                }
                }
                if (isSelectedProd == false) {
                  $scope.selectedDepartment = $scope.productDetails[-1];
                    $rootScope.productN = product.product_Name;
                    if ($state.current.name == "start.searchNotFound") {
                      //  $state.reload("start.searchNotFound");
                          idealsApi.list_modelsReq(null,product.product_Name);
                    } else {
                       // $state.go("start.searchNotFound");
                          idealsApi.list_modelsReq(null,product.product_Name);
                    }
                } else {
                    for (var dept = 0; dept < department.length; dept++) {
                            for (var prod = 0; prod < department[dept].productCodes.length; prod++) {
                                 if ((department[dept].productCodes[prod].description).toUpperCase() === comapStr) {

                             for(var prodL=0; prodL<$scope.productDetails.length; prodL++)
                             {
                             if(comapStr===(($scope.productDetails[prodL].split("-")[1]).replace(" ","")).toUpperCase())
                             {
                             //if($scope.dept.selectedDepartment===undefined)
                             //{
                           //  document.getElementById('select_value').selectedIndex=prodL+1;
                             //}
                            // else
                            // {
//                             $scope.selectedDepartment = $scope.productDetails[prodL];
                            // }

                               break;
                             }
                             }
//                            idealsApi.list_modelsReq(department[dept].productCodes[prod].id,product.product_Name);
                              idealsApi.list_modelsReq(null,product.product_Name);
                          //  $scope.dept.selectedDepartment=department[dept].productCodes[prod].id + "-" + comapStr; // weritten by seem- may be used if client require.
                            isSelectedProd == false;
                            break;
                                }
                            }
                    }
                }
            }
            }

            else if(selectedTxtSearch =="" && selectedCategories !=""){
                var productPcid = $scope.productPcid;
                idealsApi.list_modelsReq(productPcid, null);
            }
            else if(selectedTxtSearch !="" && selectedCategories !="")
            {
                var productPcid = $scope.productPcid;
                idealsApi.list_modelsReq(productPcid,selectedTxtSearch);
//                $scope.enterSomeTxt = true;
//               $scope.submitFilter();
            } else { return false}
        }

        // Filter or Search from selected Categories 16-10-18
//        $scope.submitFilter = function(){
//            $scope.SearchList = $scope.productName;
//        }

        // this is the solution to keep the drop down list selection after webpage refresh
       for (var dept = 0; dept < department.length; dept++) { //Loop through each array element
            for (var prod = 0; prod <= department[dept].productCodes.length - 1; prod++) {
              if (department[dept].productCodes[prod].description === sessionStorage.getItem("selectedProdN")) { //Check if any of the 'year' value in the array match the value stored in sessionStorage
                  $scope.prodN = {}; //reset the selected scope variable
                   $scope.productName = department[dept].productCodes[prod].description; //Assign the array element with the matching year, as the selected scope element
                   break;
               }
          }
      }

        //Set watch on array element 'year' for changes/selection
        $scope.$watch("productName", function(prodN) {
            autoSearchByDepartment("","");
            sessionStorage.setItem("selectedProdN", prodN); //When an element is selected from the drop down list, its value is stored in sessionStorage
        });

        for (var dept = 0; dept < $scope.productDetails.length; dept++) { //Loop through each array element
            if ($scope.productDetails[dept] === sessionStorage.getItem("selectedDept")) { //Check if any of the 'year' value in the array match the value stored in sessionStorage
                $scope.dept = {}; //reset the selected scope variable
                $scope.selectedDepartment = $scope.productDetails[dept]; //Assign the array element with the matching year, as the selected scope element
            }
        }

    //Set watch on array element for changes/selection
    $scope.$watch("selectedDepartment", function(dept) {
        if(dept!=undefined){
            if( $scope.selectedDepartment !=sessionStorage.getItem("selectedDept")){
            sessionStorage.removeItem('list_inventory');
            }
            sessionStorage.setItem("selectedDept", dept);
            $rootScope.departmentName = sessionStorage.getItem("selectedDept");
            autoSearchByDepartment((dept.split("-")[0]).replace(" ",""),dept);
            //          $scope.startForm.prodName.$dirty = false;
            //          $scope.startForm.prodName.$pristine = true;
            //When an element is selected from the drop down list, its value is stored in sessionStorage

                $scope.startTimer = function() {
                    mytimeout = $timeout(function() {
                    $rootScope.IsProductAvbl = false;
                    }, 1500);
                };
                $scope.startTimer();
        }
    });

    } catch (e) {
        console.log("error on start page " + e);
    }
}]);