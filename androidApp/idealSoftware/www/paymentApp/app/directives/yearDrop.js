﻿
angular.module("app")
    .directive("yearDrop", function ($rootScope) {
        function getYears(offset, range) {
            var currentDateTime = sessionStorage.currentTimeStamp;
            var currentYear = currentDateTime.substring(0, 4);
        var years = [];
        for (var index = 0; index < range + 1; index++) {
            years.push(parseInt(currentYear) +index);
        }
        return years;
    }
    return {
        link: function (scope, element, attrs) {
            scope.years = getYears(+attrs.offset, +attrs.range);
            scope.selected = scope.years[0];

            console.log(scope.selected);
        }
    }
});