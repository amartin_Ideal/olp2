angular.module("app")
    .controller("payDepositCtrl", ["$scope", "$rootScope", "$state", "idealsApi","configDetailsProvider","$locale", function($scope, $rootScope, $state, idealsApi,configDetailsProvider,$locale) {
        try {
            // check api response
            var cart;
            var minimumDepositAmount;
            var depositAmount

            $scope.maxlength = 16;
            $scope.cvvLength = 3;

            $rootScope.IsVisible = false;
            $scope.depositAmount = 0;

            // customer information
            $scope.firstName = "";
            $scope.middleName = "";
            $scope.lastName = "";
            $scope.homePhoneNumber = "";
            $scope.cellPhoneNumber = "";
            $scope.workPhoneNumber = "";
            $scope.address = "";
            $scope.apt = "";
            $scope.city = "";
            $scope.email = "";
            $scope.birthdate = "";
            $scope.state = "";
            $scope.zipCode = "";
            $scope.four = "";
            $scope.driverLicence = "";
            $scope.dlState = "";

            // employee info
            $scope.employeeName = "";
            $scope.employeePhone = "";
            $scope.employeeAddress = "";
            $scope.employeeCity = "";
            $scope.employeeState = "";
            $scope.employeeZipCode = "";
            $scope.employeeSalary = "";
            $scope.per = "";

            // refernece
            $scope.refName = "";
            $scope.refPhone = "";
            $scope.refAddress = "";
            $scope.refState = "";
            $scope.refCity = "";
            $scope.refZipCode = "";
            $scope.refRelation = "";

            // card detail
            $scope.card_type = "";
            $scope.cardholder_Name = "";
            $scope.card_Number = "";
            $scope.exp_month = "";
            $scope.exp_year = "";
            $scope.cvv_code = "";
            $scope.zip_postal_code = "";


            $scope.example = {
                value: new Date(2013, 9, 22)
            };

            $scope.currentYear = new Date().getFullYear()
            $scope.currentMonth = new Date().getMonth() + 1
            var datetime = $locale.DATETIME_FORMATS; //get date and time formats
            var format = $locale['DATETIME_FORMATS']['medium'];
            $scope.months = datetime.SHORTMONTH; //access localized months
            console.log($scope.months);

            // verify card information for making payment
            $scope.verifyCard = function(card_type) {

                $scope.cardholder_Name = "";
                $scope.card_Number = "";
                $scope.exp_month = "";
                $scope.exp_year = "";
                $scope.cvv_code = "";
                $scope.zip_postal_code = "";


                $scope.paymentForm.cardholder_Name.$dirty = false;
                $scope.paymentForm.cardholder_Name.$pristine = true;
                $scope.paymentForm.card_Number.$dirty = false;
                $scope.paymentForm.card_Number.$pristine = true;
                $scope.paymentForm.cvv_code.$dirty = false;
                $scope.paymentForm.cvv_code.$pristine = true;
                $scope.paymentForm.zip_postal_code.$dirty = false;
                $scope.paymentForm.zip_postal_code.$pristine = true;
                $scope.paymentForm.cardholder_Name.$dirty = false;
                $scope.paymentForm.cardholder_Name.$pristine = true;
                $scope.paymentForm.$submitted = false;

                if (card_type == "American Express") {
                    $scope.cardLength = 15;
                    $scope.cvvLength = 4;
                } else {
                    $scope.cardLength = 16;
                    $scope.cvvLength = 3;
                }
            }

         // Cardtype
         $scope.cardTypes = [
             "Visa",
             "Master Card",
             "American Express",
             "Discover"
         ]

            if (sessionStorage["cartInfo"] != "") {

                var count = 0;
                var countMinDeposit = 0;
                $scope.cart = JSON.parse(sessionStorage["cartInfo"] || "{}");
                console.log($scope.cart);
                console.log("Cart >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + JSON.stringify($scope.cart));

                for (var item = 0; item < $scope.cart.length; item++) {
                   // count = count + parseFloat($scope.cart[item].price * $scope.cart[item].quantity);
                    countMinDeposit = countMinDeposit + parseFloat($scope.cart[item].minimumDeposit);
                };

               // $scope.depositAmount = (count).toFixed(2);
                $scope.MinimumDepositAmount =  (countMinDeposit).toFixed(2);
            };

            //make payment after filling the card information
            $scope.makePayment = function() {
                $("#someHiddenDiv").hide();

                if ($scope.paymentForm.$valid) {

                var myNumber = $scope.exp_month;
                var formattedNumber = ("0" + myNumber).slice(-2);
                $scope.exp_month = formattedNumber;

                    $rootScope.ShowLoader = true;
                    var apiData= configDetailsProvider.apiConnect;
                    // SharedService.writePayezzyCookie();
                    // var cookieWObject = apiData.payeezy_auth;
                    var apiKey = apiData.api_key;
                    var js_security_key = apiData.payeezy_key;
                    var ta_token = apiData.ta_token;
                    var auth = true;
                    Payeezy.setApiKey(apiKey);
                    Payeezy.setJs_Security_Key(js_security_key);
                    Payeezy.setTa_token(ta_token);
                    Payeezy.setAuth(auth);
                    Payeezy.createToken(responseHandler, apiData.env);
                    $("#someHiddenDiv").show();
                    return false;

                }
            };

            // get response from payeeze server
            var responseHandler = function(status, response) {

                $("#someHiddenDiv").hide();
                if (status != 201) {
                    $("#someHiddenDiv").show();
                    if (response.error && status != 400) {
                        var error = response["error"];
                       // var errormsg = error["messages"];
                      //  var errorMessages = JSON.stringify(errormsg[0].description, null, 4);
                        $("#payment-errors").html("Error Messages:" + error);
                    }
                    if (status == 400) {
                        $("#payment-errors").html("");
                        var errormsg = response.Error.messages;
                        var errorMessages = "";
                        for (var i in errormsg) {
                            var eMessage = errormsg[i].description;
                            errorMessages = errorMessages + "Error Messages:" + eMessage
                        }
                        $("#payment-errors").html(errorMessages);
                    }
                    $("#imageDiv").hide();
                    $("#payment-form").prop("disabled", false);
                } else {
                    $("#someHiddenDiv").hide();
                    var result = response.token.value;
                    $("#payment-form").prop("disabled", true);

                    var zip_postalCode = $scope.zip_postal_code;
                    var cvvCode = $scope.cvvCode;
                    var expYear = $scope.exp_year;
                    var expDate = $scope.exp_month;
                    var cardNumber = $scope.card_Number;
                    var cardholderName = $scope.cardholder_Name;
                    var cardType = $scope.card_type;
                    var cardtoken = result;

                    var expDate = expDate + expYear;
                    var first6 = cardNumber.substr(0, 6);

                    var last4 = cardNumber.substr(12, 4);
                    var customer = {
                        firstName: $scope.firstName,
                        middleName: $scope.middleName,
                        lastName: $scope.lastName,
                        homePhoneNumber: $scope.homePhoneNumber,
                        cellPhoneNumber: $scope.cellPhoneNumber,
                        workPhoneNumber: $scope.workPhoneNumber,
                        address: $scope.address,
                        apt: $scope.apt,
                        city: $scope.city,
                        email: $scope.email,
                        birthdate: $scope.birthdate,
                        state: $scope.state,
                        zipCode: $scope.zipCode,
                        four: $scope.four,
                        driverLicence: $scope.driverLicence,
                        dlState: $scope.dlState
                    };

                    var emailSddress = customer.email;
                    console.log(emailSddress.toUpperCase());
                    var reference = {
                        refName: $scope.refName,
                        refPhone: $scope.refPhone,
                        refAddress: $scope.refAddress,
                        refState: $scope.refState,
                        refCity: $scope.refCity,
                        refZipCode: $scope.refZipCode,
                        refRelation: $scope.refRelation
                    };

                    var employee = {
                        employeeName: $scope.employeeName,
                        employeePhone: $scope.employeePhone,
                        employeeAddress: $scope.employeeAddress,
                        employeeCity: $scope.employeeCity,
                        employeeState: $scope.employeeState,
                        employeeZipCode: $scope.employeeZipCode,
                        employeeSalary: $scope.employeeSalary,
                        per: $scope.per
                    };

                    sessionStorage.firstName = customer.firstName;
                    sessionStorage.lastName = customer.lastName;
                    sessionStorage.address = customer.address;
                    sessionStorage.address = customer.address;
                    sessionStorage.city = customer.city;
                    sessionStorage.state = customer.state;
                    sessionStorage.zipCode = customer.zipCode;
                    sessionStorage.email = customer.email;
                    sessionStorage.phoneNumber = customer.homePhoneNumber;
                    prodItems=$scope.cart;
                    depositAmount=$scope.depositAmount;
                    minimumDepositAmount=$scope.MinimumDepositAmount;
                    var proceType='entry';
                    setPayDepositDetails(customer,reference,employee,prodItems,depositAmount,minimumDepositAmount,proceType);
                    idealsApi.collect_depositReq(customer, reference, employee, prodItems, cardtoken, cardType, expDate, cardholderName, first6, last4, depositAmount, minimumDepositAmount,proceType);
                    $("payment-form").prop("disabled", false);
                }
                $rootScope.ShowLoader = false;

                //..................android and ios back button diable ..................
                document.addEventListener("deviceready", onDeviceReady, false);

                function onDeviceReady() {
                    document.addEventListener("backbutton", function(e) {
                        e.preventDefault();
                    }, true);
                }

            };

           function setPayDepositDetails(customer,reference,employee,prodItems,depositAmount,MinimumDepositAmount,proceType)
            {
              var data1={
                        "items":prodItems,
                        "customerinfo": customer,
                        "reference": reference,
                        "employeeInfo":employee,
                        "depositAmount": depositAmount,
                        "MinimumDepositAmount": MinimumDepositAmount,
                        "proceType": proceType
                        };
              sessionStorage["PayDepositDetails"] = JSON.stringify(data1);
            }

        } catch (e) {
            //$form.find('imageDiv').prop('disabled', true);
            console.log(e);

            //............android and ios back button diable ...............
            document.addEventListener("deviceready", onDeviceReady, false);
            function onDeviceReady() {
                document.addEventListener("backbutton", function(e) {
                    e.preventDefault();
                }, false);
            }

        }
    }]);
