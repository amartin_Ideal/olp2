﻿
angular.module("app")
.constant('filterItem', {
         cashPriceLH: 'Cash Price Low to High',
         cashPriceHL: 'Cash Price High to Low',
         weeklyRateLH: 'Weekly Rate Low to High',
         weeklyRateHL: 'Weekly Rate High to Low',
         monthlyRateLH: 'Monthly Rate Low to High',
         monthlyRateHL: 'Monthly Rate High to Low',
         fPriceLH: 'price',
         fPriceHL: '-price',
         fWeeklyLH: 'weeklyRate',
         fWeeklyHL: '-weeklyRate',
         fRateLH: 'monthlyRate',
         fRateHL: '-monthlyRate'
 });

