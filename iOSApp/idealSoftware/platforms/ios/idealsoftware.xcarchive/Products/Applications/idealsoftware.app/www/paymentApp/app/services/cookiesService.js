﻿angular.module("app")
    .service("Apicookies", ["$rootScope", "$cookieStore", "$location", function($rootScope, $cookieStore, $location) {
        try {
            this.writeApiCookie = function() {
                var report_url = "";
                if (sessionStorage.report_url) {
                    report_url = JSON.parse(sessionStorage["report_url"] || "{}");
                } else {
                    report_url = "";
                }
                var api = {
                    "token": "0cff2ea8cbffa3b887bc2891c72d91d9",
                    "key": "e398db56e895c3a999c321b28785eedb",
                    "group": "241",
                    "api": "vr_store",
                    "client_url": "https://pubsub-test.idealss.net/faye",
                    "report_url": "https://test.idealss.net/report_a_problem",
                    "location_url": "chetu1",
                    "tac_url": "https://static.test.idealss.net",
                    "location_id": 1,
                    "title": "Chetu, Inc.",
                    "env": "development"
                };
                ($cookieStore.put("api_connect", api));
            };

            this.writePayezzyCookie = function() {
                var auth = {
                    "payeezy_key": "js-6125e57ce5c46e10087a545b9e9d7354c23e1a1670d9e9c7",
                    "api_key": "y6pWAJNyJyjGv66IsVuWnklkKUPFbb0a",
                    "ta_token": "NOIW"
                };
                $cookieStore.put("payeezy_auth", auth);
            }
            //function to remove cookie
            this.removeCookie = function() {

                $cookieStore.remove("api_connect");
                $cookieStore.remove("payeezy_auth");
            };
        } catch (e) {
            console.log(e);
        }
    }]);