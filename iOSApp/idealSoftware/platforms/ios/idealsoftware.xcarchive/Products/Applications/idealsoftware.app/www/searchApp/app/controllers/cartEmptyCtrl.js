﻿angular.module("app")
    .controller("cartEmptyCtrl", ["$scope", "$rootScope", "$state", "idealsApi", "$cookieStore", "$window", function($scope, $rootScope, $state, idealsApi, $cookieStore, $window) {
        try {

            $rootScope.IsVisible = false;
            //back to start page
            $scope.goback = function() {
                $window.history.back();
            }
        } catch (e) {
            sessionStorage["cartInfo"]
        }
    }]);